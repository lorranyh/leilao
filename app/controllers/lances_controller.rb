class LancesController < ApplicationController
    def create
        @evento = Evento.find(params[:evento_id])

        @lance = @evento.lances.create(lance_params)

        redirect_to evento_path(@evento)
    end

    private
        def lance_params
            params.require(:lance).permit(:nome,:valor)
        end
end
