class Evento < ApplicationRecord
    #belongs_to :user
    has_many :lances
    validates  :marca, presence: true
    validates  :modelo, presence: true
    validates  :ano, presence: true
    validates  :combustivel, presence: true
    validates  :valor, presence: true
end