json.extract! evento, :id, :status, :usuario, :marca, :modelo, :ano, :combustivel, :valor, :fim, :created_at, :updated_at
json.url evento_url(evento, format: :json)
