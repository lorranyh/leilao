Rails.application.routes.draw do
  resources :eventos
  resources :user_sessions
  resources :users
  
  get 'login' => 'user_sessions#new', :as => :login
  post 'logout' => 'user_sessions#destroy', :as => :logout

  resources :users
  resources :eventos do
    resources :lances
  end
  root 'eventos#index'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
