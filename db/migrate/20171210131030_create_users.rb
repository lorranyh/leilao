class CreateUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      t.string :nome,              :null => false
      t.string :sobrenome,         :null => false
      t.string :email,             :null => false
      t.string :crypted_password,  :null => false
      t.string :salt

      t.timestamps
    end
  end
end
