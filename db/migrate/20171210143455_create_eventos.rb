class CreateEventos < ActiveRecord::Migration[5.1]
  def change
    create_table :eventos do |t|
      t.string :status
      t.string :usuario
      t.string :marca
      t.string :modelo
      t.integer :ano
      t.string :combustivel
      t.float :valor
      t.time :fim

      t.timestamps
    end
  end
end
