class CreateLances < ActiveRecord::Migration[5.1]
  def change
    create_table :lances do |t|
      t.string :nome
      t.float :valor
      t.references :evento, foreign_key: true

      t.timestamps
    end
  end
end
