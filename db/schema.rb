# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20171210152756) do

  create_table "eventos", force: :cascade do |t|
    t.string "status"
    t.string "usuario"
    t.string "marca"
    t.string "modelo"
    t.integer "ano"
    t.string "combustivel"
    t.float "valor"
    t.time "fim"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "lances", force: :cascade do |t|
    t.string "nome"
    t.float "valor"
    t.integer "evento_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["evento_id"], name: "index_lances_on_evento_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "nome", null: false
    t.string "sobrenome", null: false
    t.string "email", null: false
    t.string "crypted_password", null: false
    t.string "salt"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
